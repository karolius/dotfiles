" ============= Vim-Plug ============== "{{{
" auto-install vim-plug {
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin(expand('~/.config/nvim/plugged'))
"}

" looks and GUI stuff {
Plug 'vim-airline/vim-airline'                          " airline status bar
Plug 'ryanoasis/vim-devicons'                           " pretty icons everywhere
Plug 'luochen1990/rainbow'                              " rainbow parenthesis
Plug 'hzchirs/vim-material'                             " material color themes
Plug 'gregsexton/MatchTag'                              " highlight matching html tags
Plug 'scrooloose/nerdtree'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'Xuyuanp/nerdtree-git-plugin'
"}

" functionalities {
Plug 'neoclide/coc.nvim', {'branch': 'release'}         " LSP and more
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }     " fzf itself
Plug 'junegunn/fzf.vim'                                 " fuzzy search integration
Plug 'SirVer/ultisnips'                                 " snippets manager
Plug 'honza/vim-snippets'                               " actual snippets
"Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}  " better python
Plug 'tpope/vim-commentary'                             " better commenting
Plug 'tpope/vim-fugitive'                               " git support
Plug 'wellle/tmux-complete.vim'                         " complete words from a tmux panes
Plug 'christoomey/vim-tmux-navigator'                   " seamless vim and tmux navigation
Plug 'tpope/vim-eunuch'                                 " run common Unix commands inside Vim
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'tpope/vim-abolish'                                " working with variants of a word
Plug 'scrooloose/nerdcommenter'                         " ++ to comment
Plug 'alvan/vim-closetag'                               " html tags autoclose
Plug 'tpope/vim-surround'                               " ---
Plug 'andrewradev/splitjoin.vim'                        " split inline array like with gS
Plug 'tpope/vim-unimpaired'                             " complementary pairs of mappings
"Plug 'psliwka/vim-smoothie'
"}

" langs {
Plug 'sheerun/vim-polyglot'                             " syntax support
"}

call plug#end()
"}}}

" ==================== general config ======================== "{{{
set background=dark
set t_Co=256
" necessary if use 'set termguicolors'.
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
" set mouse=a                                             " enable mouse scrolling
set clipboard+=unnamedplus                              " use system clipboard by default
filetype plugin indent on                               " enable indentations
set tabstop=4 softtabstop=4 shiftwidth=4 autoindent     " tab width
set expandtab smarttab                                  " tab key actions
set incsearch ignorecase smartcase hlsearch             " highlight text while searching
set list listchars=trail:»,tab:»-,extends:#,nbsp:.      " use tab to navigate in list mode
set fillchars+=vert:\▏                                  " requires a patched nerd font
set wrap breakindent                                    " wrap long lines to the width set by tw
set encoding=utf-8                                      " text encoding
set number                                              " enable numbers on the left
set title                                               " tab title as file name
set noshowmode                                          " dont show current mode below statusline
set conceallevel=2                                      " set this so we wont break indentation plugin
set splitright splitbelow                               " open vertical/horizontal split to the right/bottom
set tw=120                                              " auto wrap lines that are longer than that
set emoji                                               " enable emojis
set history=1000                                        " history limit
set autowrite autoread                       		    " auto write/read a file when leaving a modified buffer
set virtualedit=onemore             					" allow for cursor beyond last character
set backspace=indent,eol,start                          " sensible backspacing
set undofile                                            " enable persistent undo
set undodir=/tmp                                        " undo temp file directory
set foldlevel=0                                         " open all folds by default
set inccommand=nosplit " setreg('A', 'foo') doubled bug " visual feedback while substituting
set showtabline=2                                       " always show tabline
set grepprg=rg\ --vimgrep                               " use rg as default grepper
set cursorline
set tabpagemax=15

" performance tweaks
" set nocursorline
set scrolljump=4
set lazyredraw
set redrawtime=10000
set synmaxcol=180
set re=1

" required by coc
set hidden
set nobackup
set nowritebackup
set cmdheight=1
set updatetime=300                                      " better for diagnostic msg than 4s
set shortmess+=c
set signcolumn=yes

" Themeing
let g:material_style = 'dark'
colorscheme vim-material

""" Pmenu colors
hi Pmenu ctermfg=0 ctermbg=6 guibg=#36434d
hi PmenuSbar guibg=#bcbcbc
hi PmenuSel ctermfg=7 ctermbg=4 guibg=#313237 guifg=#955550
hi PmenuThumb guibg=#585858

autocmd ColorScheme * hi VertSplit cterm=NONE           " split color
hi Comment gui=italic cterm=italic               " bold comments
hi CursorLine ctermbg=DarkBlue guibg=#1a2227
hi CursorLineNr gui=bold                                " make relative number bold
hi NonText guifg=bg                                     " mask ~ on empty lines
hi Search guibg=#b16286 guifg=#ebdbb2 gui=NONE          " search string hi color
hi SpellBad guifg=#ff7480 cterm=bold,undercurl          " misspelled words
hi clear CursorLineNr                                   " use the theme color for relative number

" git (gutter)
hi DiffAdd  guibg=#27323a guifg=#43a047
hi DiffChange guibg=#27323a guifg=#fdd835
hi DiffRemoved guibg=#27323a guifg=#e53935

" coc multi cursor hi color
hi CocCursorRange guibg=#b16286 guifg=#ebdbb2
"}}}

" ======================== Plugin Configurations ======================== "{{{
" vim buildins {
    "let g:python3_host_prog = $WORKON_HOME.'/fapi-dev/bin/python'   " set dedicated virtualenv
    let g:loaded_python_provider = 0
    let g:loaded_perl_provider = 0
    let g:loaded_ruby_provider = 0
    let g:omni_sql_no_default_maps = 1                          " disable sql omni completion
"}

" Airline {
    let g:airline_theme='material'
    let g:airline_powerline_fonts = 0
    let g:airline#themes#clean#palette = 1
    call airline#parts#define_raw('linenr', '%l')
    call airline#parts#define_accent('linenr', 'bold')
    let g:airline_section_z = airline#section#create(['%3p%%  ',
                \ g:airline_symbols.linenr .' ', 'linenr', ':%c '])
    let g:airline_section_warning = ''
    let g:airline#extensions#tabline#enabled = 1
    let g:airline#extensions#tabline#buffer_min_count = 2   " show tabline only if there is more than 1 buffer
    let g:airline#extensions#tabline#fnamemod = ':t'        " show only file name on tabs
"}

" coc {
  " Navigate snippet placeholders using tab
  let g:coc_snippet_next = '<Tab>'
  let g:coc_snippet_prev = '<S-Tab>'

  " list of the extensions to make sure are always installed
  let g:coc_global_extensions = [
      \'coc-actions',
      \'coc-cspell-dicts',
      \'coc-css',
      \'coc-git',
      \'coc-html',
      \'coc-json',
      \'coc-lists',
      \'coc-pairs',
      \'coc-pyright',
      \'coc-ultisnips',
      \'coc-spell-checker',
      \'coc-stylelintplus',
      \'coc-tsserver',
      \'coc-yaml',
      \'coc-eslint',
      \'coc-prettier',
      \'coc-vetur',
      \'coc-highlight',
      \]
      "\'coc-snippets',
      " \'coc-syntax',
      " \'coc-python',
"}

" FZF {
  let g:fzf_action = {
    \ 'ctrl-t': 'tab split',
    \ 'ctrl-x': 'split',
    \ 'ctrl-v': 'vsplit' }

  let g:fzf_layout = {
    \ 'up': '~90%',
    \ 'window': {
      \ 'width': 0.8,
      \ 'height': 0.8,
      \'yoffset': 0.5,
      \'xoffset': 0.5,
      \'border': 'rounded'
      \ }
    \ }

  let g:fzf_colors =
  \ { 'fg':      ['fg', 'Comment'],
    \ 'border':  ['fg', 'Comment'] }

  let g:fzf_tags_command = 'ctags -R'

  let $FZF_DEFAULT_OPTS = '--layout=reverse --inline-info'
  let $FZF_DEFAULT_COMMAND = "rg --files --hidden --glob '!.git/**'"
" }

" closetag {
    " These are the file extensions where this plugin is enabled
    let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.vue'
    let g:closetag_filetypes = 'html,xhtml,phtml,vue'
    let g:closetag_xhtml_filetypes = 'xhtml,jsx'

    " This will make the list of non-closing tags case-sensitive
    " (e.g. `<Link>` will be closed while `<link>` won't.)
    let g:closetag_emptyTags_caseSensitive = 1

    " Disables auto-close if not in a 'valid' region (based on filetype)
    let g:closetag_regions = {
        \ 'typescript.tsx': 'jsxRegion,tsxRegion',
        \ 'javascript.jsx': 'jsxRegion',
        \ }

    " Add > at current position without closing the current tag, default is ''
    let g:closetag_close_shortcut = '<leader>>'
" }

let g:NERDTreeIgnore = ['^node_modules$']
let g:NERDTreeGitStatusWithFlags = 1
let g:rainbow_active = 1                                " rainbow brackets
let g:tmux_navigator_no_mappings = 1                    " tmux navigator

" performance issue fix in vue files
let g:vue_pre_processors = []
autocmd FileType vue syntax sync fromstart
"}}}

" ================== Functions ===================== "{{{
" advanced grep(faster with preview)
function! RipgrepFzf(query, fullscreen)
    let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case %s || true'
    let initial_command = printf(command_fmt, shellescape(a:query))
    let reload_command = printf(command_fmt, '{q}')
    let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command], 'dir': system('git rev-parse --show-toplevel 2> /dev/null')[:-2]}
    call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

" check if last inserted char is a backspace (used by coc pmenu)
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" show docs on things with K
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" moves cursor past common wrappers
function! PastFirstPaired()
    if search('[\])}"'."']", 'e')
      call feedkeys("\a")
    endif
endfunction

" FZF
function! s:getVisualSelection()
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)

    if len(lines) == 0
        return ""
    endif

    let lines[-1] = lines[-1][:column_end - (&selection == "inclusive" ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]

    return join(lines, "\n")
endfunction

" * and # in the Visual mode will search the selected text
function! s:VisualStarSearch(search_cmd)
    let l:tmp = @"
    normal! gvy
    let @/ = '\V' . substitute(escape(@", a:search_cmd . '\'), '\n', '\\n', 'g')
    let @" = l:tmp
endfunction

function! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfunction
"}}}

" ======================== Commands ============================= "{{{
" automatically reload init.vim when it's saved
au BufWritePost init.vim so ~/.config/nvim/init.vim

" switch to current file dir when a new buffer is opened (TOFIX: cdo throw path errors)
" autocmd BufEnter * if bufname("") !~ "^\[A-Za-z0-9\]*://" | lcd %:p:h | endif
" au FileType help wincmd L                           " open help in vertical split
" au BufWritePre * :%s/\s\+$//e                       " remove trailing whitespaces before saving
au BufEnter * set fo-=c fo-=r fo-=o                 " stop annoying auto commenting on new lines

" coc completion popup
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
" disable completion in command-line window
autocmd CmdwinEnter * let b:coc_suggest_disable = 1

" fzf if passed argument is a folder
augroup folderarg
    " change working directory to passed directory
    autocmd VimEnter * if argc() != 0 && isdirectory(argv()[0]) | execute 'cd' fnameescape(argv()[0])  | endif

    " fallback if fzf is closed
    autocmd VimEnter * if argc() != 0 && isdirectory(argv()[0]) | endif

    " start fzf on passed directory
    autocmd VimEnter * if argc() != 0 && isdirectory(argv()[0]) | execute 'Files ' fnameescape(argv()[0]) | endif
augroup END

" Return to last edit position when opening files
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" mappings are added on VimEnter to override mappings from the
" vim-indexed-search plugin
augroup vimrc-editing-visual-star-search
    autocmd!
    autocmd VimEnter *
        \ xmap * :<C-u>call <SID>VisualStarSearch('/')<CR>n
        \|xmap # :<C-u>call <SID>VisualStarSearch('?')<CR>N
augroup END

" files in fzf
command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({'options': ['--layout=reverse', '--inline-info']}), <bang>0)

" advanced grep
command! -nargs=* -bang Rg call RipgrepFzf(<q-args>, <bang>0)

au BufWritePost * :call TrimWhitespace()
"}}}

" ======================== Mappings ====================== "{{{
"" the essentials vim: reload, open: init.vim, coc-config
nmap <leader>vr :so ~/.config/nvim/init.vim<CR>
nmap <leader>vi :edit ~/.config/nvim/init.vim<CR>
nmap <leader>vc :<C-u>CocConfig<cr>

nmap <Tab> :bnext<CR>
nmap <S-Tab> :bprevious<CR>

" new line in normal mode and back
nmap <Enter> o
nmap <S-Enter> o<C-c>

" use a different register for delete and paste
" nnoremap d "_d
" vnoremap d "_d
" vnoremap p "_dP
" nnoremap x "_x

" emulate windows copy, cut behavior
vnoremap <LeftRelease> "+y<LeftRelease>
vnoremap <C-c> "+y<CR>
vnoremap <C-x> "+d<CR>

" insertmode vimlike motions: ctrl + {h,j,k,l}
inoremap <C-k> <C-o><up>
inoremap <C-j> <C-o><down>
inoremap <C-h> <C-o><left>
inoremap <C-l> <C-o><right>

" commandline shelllike motions
cmap <A-f> <S-right>
cmap <A-b> <S-left>
cmap <C-l> <C-e><C-u>

" show mapping on all modes with F8
nmap <F1> <plug>(fzf-maps-n)
imap <F1> <plug>(fzf-maps-i)
vmap <F1> <plug>(fzf-maps-x)
"au FileType help wincmd L                           " open help in vertical split

" Clear search highlighting rather than toggle it on and off
nmap <silent> <leader>' :nohlsearch<CR>
" Toggle search highlighting rather than clear the current search results
nmap <silent> <leader>/ :set invhlsearch<CR>
" Search for visually selected text
vmap // y/\V<C-r>=escape(@",'/\')<CR><CR>")
" search inside a visual selection
xmap / <Esc>/\%><C-R>=line("'<")-1<CR>l\%<<C-R>=line("'>")+1<CR>l
xmap ? <Esc>?\%><C-R>=line("'<")-1<CR>l\%<<C-R>=line("'>")+1<CR>l

" phrase search with selection
"vnoremap <silent><leader>f <Esc>:Rg <C-R>=<SID>getVisualSelection()<CR><CR>
nmap <leader>p :Files<CR>
nmap <leader>P :Rg <C-R>=expand("<cword>")<CR><CR>
nmap <leader>h :History/<CR>
nmap <leader>b :Buffers<CR>
nmap <leader>t :BTags<CR>
" display all lines with keyword under cursor and ask which one to jump to
nmap <leader>w [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>
nmap <leader>cm :Commands<CR>
nmap <leader>gc :Commits<CR>
nmap <leader>gf :GFiles<CR>
nmap <leader>gu :GFiles?<CR>
" find MERGE CONFLICTS markers
map <leader>gm /\v^[<\|=>]{7}( .*\|$)<CR>
" fugitive mappings
nmap <leader>gd :Gdiffsplit<CR>
nmap <leader>gb :Gblame<CR>

" snippets
nmap <leader>se  :<C-u>UltiSnipsEdit<CR>
" coc-snippets utils, not available in ultisnips
"nmap <leader>so  :<C-u>CocCommand snippets.openSnippetFiles<cr>
"xmap <leader>sc  <Plug>(coc-convert-snippet)

" <c-s> to not conflict with next
let g:UltiSnipsExpandTrigger="<C-s>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" tab to navigate snippet placeholders
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-l>" : "\<C-h>"

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use enter to accept snippet expansion
"inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" multi cursor shortcuts
"nmap <silent> <C-a> <Plug>(coc-cursors-word)
"xmap <silent> <C-a> <Plug>(coc-cursors-range)

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <silent> ]n <Plug>(coc-diagnostic-next) <bar> :CocAction<CR>

" organize imports
nmap <leader>o :<C-u>CocCommand pyright.organizeimports<cr>

" rename
nmap <silent><leader>rn <Plug>(coc-rename)
nmap <silent><leader>rf  :<C-u>CocCommand workspace.renameCurrentFile<cr>

" python renaming
"autocmd FileType python nnoremap <leader>rn :Semshi rename <CR>

" jump stuff
nmap <leader>jd <Plug>(coc-definition)
nmap <leader>jy <Plug>(coc-type-definition)
nmap <leader>ji <Plug>(coc-implementation)
nmap <leader>jr <Plug>(coc-references)

" other coc actions
xmap <silent> <leader>a :<C-u>execute 'CocAction ' . visualmode()<CR>
nmap <leader>a :CocAction<CR>
nmap <leader>aw  :<C-u>CocCommand cSpell.addWordToUserDictionary<cr><cr>
nmap <silent> K :call <SID>show_documentation()<CR>

" Remap keys for applying codeAction to the current buffer.
noremap <leader>ca  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" open/close quickfix window (debugging compile errors)
nmap <leader>co :copen<CR>
noremap <leader>cc :cclose<CR>
nmap <leader>cn :cn<CR>
nmap <leader>cp :cp<CR>
nmap <leader>cf :cnf<CR>

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" floating window glitch fix
inoremap <silent><C-c> <ESC>:call coc#float#close_all()<CR>
nnoremap <silent><C-c> <Esc>:call coc#float#close_all()<CR>
"use cut c-c x vnoremap <silent><C-c> <Esc>:call coc#float#close_all()<CR>

" tmux navigator
nnoremap <silent> <C-l> :TmuxNavigateRight<cr>
nnoremap <silent> <C-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-j> :TmuxNavigateDown<cr>
nnoremap <silent> <C-k> :TmuxNavigateUp<cr>

" w|bd
command! Wd write|bdelete
command! WD write|bdelete
cnoreabbrev wd Wd
" Stupid shift key fixes
command! -bang -nargs=* -complete=file E e<bang> <args>
command! -bang -nargs=* -complete=file W w<bang> <args>
command! -bang -nargs=* -complete=file Wq wq<bang> <args>
command! -bang -nargs=* -complete=file WQ wq<bang> <args>
command! -bang -nargs=* -complete=file Bd bd<bang> <args>
command! -bang -nargs=* -complete=file Bd bd<bang> <args>
command! -bang Wa wa<bang>
command! -bang WA wa<bang>
command! -bang Q q<bang>
command! -bang QA qa<bang>
command! -bang Qa qa<bang>

" When you forget to sudo write the file
cmap w!! w !sudo tee % >/dev/null
" change Working Directory to that of the current file
cmap cwd lcd %:p:h
cmap cd. lcd %:p:h

" stay in the Visual mode when using shift commands
vmap < <gv
vmap > >gv
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" Y/C/D from the cursor to the end of the line
nnoremap Y y$
nnoremap D d$
nnoremap C c$

" Select the pasted text character-wise
nnoremap <expr> gp '`[' . strpart(getregtype(), 0, 1) . '`]'

" go past first paired matching and stay in insert mode
imap <silent><S-Tab> <C-c>:call PastFirstPaired()<CR>

nmap <C-n> :NERDTreeToggle<CR>

" NERDCommenter {
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle
" }
"}}}

" info: check:
" - gruvbox theme vim+bat


nmap <leader>1 :call coc#config('diagnostic.messageTarget', 'echo')<CR>
nmap <leader>2 :call coc#config('diagnostic.messageTarget', 'float')<CR>
