#!/bin/bash
# pgadmin packages
curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sudo sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'

# postgres
# https://www.digitalocean.com/community/tutorials/how-to-install-postgresql-on-ubuntu-20-04-quickstart
sudo apt install postgresql postgresql-contrib pgadmin4 postgis postgresql-12-postgis-3 -y
# default user postgres none pass
# connection_url = 'postgresql+psycopg2://user:password@localhost:5432/db_name' // or non +psycopg2


# add psql packages https://www.postgresql.org/download/linux/ubuntu/
# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
# Update the package lists:
sudo apt-get update
# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql


# py project livbs fix
sudo apt-get install -y build-essential python-dev libssl-dev libffi-dev libpq-dev libjpeg-dev zlib1g-dev # python3.8-dev
# pip install setuptools==45
# if package like psycopg2 fails, then install new version and update the requirements with it
# in dp run: CREATE EXTENSION postgis;
