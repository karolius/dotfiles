# curl -o init.sh https://bitbucket.org/karolius/dotfiles/raw/master/install.sh && source init.sh
sudo apt install -y software-properties-common -y
sudo add-apt-repository ppa:neovim-ppa/stable -y
sudo apt update -y
sudo apt install -y git curl xclip zsh powerline fonts-powerline tmux ripgrep watchman
# nvim
sudo apt install -y python-dev python-pip python3-dev python3-pip neovim

# py utils
sudo pip3 install pipenv virtualenv virtualenvwrapper
curl -Lks https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | bash
sed -i "s/\/usr\/local/\$HOME\/.local/g" ~/.oh-my-zsh/plugins/virtualenvwrapper/virtualenvwrapper.plugin.zsh
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# dotfiles
git clone --bare https://karolius@bitbucket.org/karolius/dotfiles.git $HOME/.cfg
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}
mkdir -p .config-backup
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
config checkout
config config status.showUntrackedFiles no

# terminal color scheme and font
curl -Lks https://raw.githubusercontent.com/Mayccoll/Gogh/master/themes/material.sh | bash
curl -O https://download.jetbrains.com/fonts/JetBrainsMono-2.001.zip
sudo unzip JetBrainsMono-2.001.zip -d /usr/share/fonts
rm JetBrainsMono-2.001.zip
# devicons
mkdir -p ~/.local/share/fonts
cd ~/.local/share/fonts && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf

# nvm
curl -Lks https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash

# bat ubuntu 20.04 workaround (ripgrep subcfg file collision)
sudo apt install -o Dpkg::Options::="--force-overwrite" bat ripgrep -y
# add custom themes for bat
batcat cache --build

# fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

source ~/.bashrc 2> /dev/null
source ~/.zshrc 2> /dev/null
tmux source-file ~/.tmux.conf
nvm install 14.12.0
npm install -g yarn
ssh-keygen
nvim
chsh -s $(which zsh)
zsh
# coc js lint: npm install --save-dev eslint prettier eslint-config-prettier eslint-plugin-prettier eslint-plugin-vue
