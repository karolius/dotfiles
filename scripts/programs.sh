#!/bin/bash
sudo add-apt-repository -y universe ppa:linuxuprising/shutter
sudo apt install -y snapd curl htop nethogs chromium-browser deja-dup steam shutter simplescreenrecorder
sudo snap install acestreamplayer clementine


# https://linuxconfig.org/how-to-install-macos-theme-on-ubuntu-20-04-focal-fossa-linux
# gtk libs
sudo apt install gnome-tweak-tool gtk2-engines-murrine gtk2-engines-pixbuf sassc libcanberra-gtk-module libglib2.0-dev -y
# https://www.gnome-look.org/p/1275087/ mojave theme
