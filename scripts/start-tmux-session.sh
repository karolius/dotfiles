#!/bin/bash

# 1 manage
tmux new-session -d -s pzesp
tmux send-keys 'cda && workon pzesp36 && flask run' 'C-m'
tmux split-window -h
tmux send-keys 'cda && workon pzesp 36 && flask shell' 'C-m'

# 2 py
tmux new-window
tmux send-keys 'cda && vim' 'C-m'
tmux split-window -h
tmux send-keys 'cda && vim' 'C-m'

# 3 npm
tmux new-window
tmux send-keys 'cdg && npm run serve' 'C-m'
tmux split-window -h
tmux send-keys 'cdg && vim' 'C-m'

# 4 js
tmux new-window
tmux send-keys 'cdg && vim' 'C-m'
tmux split-window -h
tmux send-keys 'cdg && vim' 'C-m'

# 5 npm / empty
tmux new-window
tmux send-keys 'cd /home/karolius/pzesp/CoreUI-Vue && npm run serve' 'C-m'
tmux split-window -h

tmux select-window -t 4
tmux a
